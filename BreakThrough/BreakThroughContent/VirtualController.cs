﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//test
namespace Controls
{
    public class VirtualController
    {
        public InputMotion Next;
        public InputMotion Back;
        public InputMotion Menu;
        public InputMotion Help;

        public InputMotion Act1;
        public InputMotion Act2;
        public InputMotion Act3;
        public InputMotion Act4;

        public InputMotion Up;
        public InputMotion Down;
        public InputMotion Left;
        public InputMotion Right;

        public VirtualController()
        {
            Next = InputLoader.KeyboardLoader.Z;
            Back = InputLoader.KeyboardLoader.X;
            Menu = InputLoader.KeyboardLoader.Escape;
            Help = InputLoader.KeyboardLoader.Function[0];

            Act1 = InputLoader.KeyboardLoader.Z;
            Act2 = InputLoader.KeyboardLoader.X;
            Act3 = InputLoader.KeyboardLoader.C;
            Act4 = InputLoader.KeyboardLoader.V;

            Up = InputLoader.KeyboardLoader.Up;
            Down = InputLoader.KeyboardLoader.Down;
            Left = InputLoader.KeyboardLoader.Left;
            Right = InputLoader.KeyboardLoader.Right;
        }

        public VirtualController(InputMotion next, InputMotion back,
                          InputMotion menu, InputMotion help,
                          InputMotion act1, InputMotion act2,
                          InputMotion act3, InputMotion act4,
                          InputMotion up, InputMotion down,
                          InputMotion left, InputMotion right)
        {
            Next = next;
            Back = back;
            Menu = menu;
            Help = help;

            Act1 = act1;
            Act2 = act2;
            Act3 = act3;
            Act4 = act4;

            Up = up;
            Down = down;
            Left = left;
            Right = right;
        }
    }
}
