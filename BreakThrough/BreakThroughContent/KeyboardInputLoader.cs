﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace Controls
{
    public class KeyboardInputLoader
    {
        internal KeyboardState NewKeys;
        internal KeyboardState OldKeys;

        public KeyMotion A;
        public KeyMotion B;
        public KeyMotion C;
        public KeyMotion D;
        public KeyMotion E;
        public KeyMotion F;
        public KeyMotion G;
        public KeyMotion H;
        public KeyMotion I;
        public KeyMotion J;
        public KeyMotion K;
        public KeyMotion L;
        public KeyMotion M;
        public KeyMotion N;
        public KeyMotion O;
        public KeyMotion P;
        public KeyMotion Q;
        public KeyMotion R;
        public KeyMotion S;
        public KeyMotion T;
        public KeyMotion U;
        public KeyMotion V;
        public KeyMotion W;
        public KeyMotion X;
        public KeyMotion Y;
        public KeyMotion Z;

        public KeyMotion[] Number = new KeyMotion[10];
        public KeyMotion[] Function = new KeyMotion[12];
        public KeyMotion Left;
        public KeyMotion Right;
        public KeyMotion Up;
        public KeyMotion Down;
        public KeyMotion Space;
        public KeyMotion Escape;

        internal KeyboardInputLoader()
        {
            NewKeys = Keyboard.GetState();
            OldKeys = NewKeys;

            A = new KeyMotion(65);
            B = new KeyMotion(66);
            C = new KeyMotion(67);
            D = new KeyMotion(68);
            E = new KeyMotion(69);
            F = new KeyMotion(70);
            G = new KeyMotion(71);
            H = new KeyMotion(72);
            I = new KeyMotion(73);
            J = new KeyMotion(74);
            K = new KeyMotion(75);
            L = new KeyMotion(76);
            M = new KeyMotion(77);
            N = new KeyMotion(78);
            O = new KeyMotion(79);
            P = new KeyMotion(80);
            Q = new KeyMotion(81);
            R = new KeyMotion(82);
            S = new KeyMotion(83);
            T = new KeyMotion(84);
            U = new KeyMotion(85);
            V = new KeyMotion(86);
            W = new KeyMotion(87);
            X = new KeyMotion(88);
            Y = new KeyMotion(89);
            Z = new KeyMotion(90);

            for (int i = 96; i <= 105; i++)
            {
                Number[i - 96] = new KeyMotion(i);
            }
            for (int i = 112; i <= 123; i++)
            {
                Function[i - 112] = new KeyMotion(i);
            }
            Left = new KeyMotion(37);
            Right = new KeyMotion(39);
            Up = new KeyMotion(38);
            Down = new KeyMotion(40);
            Space = new KeyMotion(32);
            Escape = new KeyMotion(27);
        }
        internal void GetInput()
        {
            OldKeys = NewKeys;
            NewKeys = Keyboard.GetState();
        }
    }
}
