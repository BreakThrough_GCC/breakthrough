﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace Controls
{
    public class MouseClickMotion : InputMotion
    {
        private int ButtonID;

        internal MouseClickMotion(int buttonID)
        {
            ButtonID = buttonID;
        }

        public override bool IsOn
        {
            get
            {
                if (InputLoader.MouseLoader.NewMouseButton[ButtonID] == true)
                    return true;
                return false;
            }
        }
        public override bool IsJustOn
        {
            get
            {
                if (InputLoader.MouseLoader.OldMouseButton[ButtonID] == false
                    && InputLoader.MouseLoader.NewMouseButton[ButtonID] == true)
                    return true;
                return false;
            }
        }
        public override bool IsJustOff
        {
            get 
            {
                if (InputLoader.MouseLoader.OldMouseButton[ButtonID] == true
                    && InputLoader.MouseLoader.NewMouseButton[ButtonID] == false)
                    return true;
                return false;
            }
        }
    }
}
