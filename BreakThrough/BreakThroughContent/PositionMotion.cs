﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controls
{
    public class PositionMotion
    {
        public float SubtractXFrom()
        {
            return InputLoader.MouseLoader.NewMouse.X
                   -InputLoader.MouseLoader.OldMouse.X;
        }
        public float SubtractXFrom(float objectX)
        {
            return InputLoader.MouseLoader.NewMouse.X -objectX;
        }

        public float SubtractYFrom()
        {
            return InputLoader.MouseLoader.NewMouse.Y
                   -InputLoader.MouseLoader.OldMouse.Y;
        }
        public float SubtractYFrom(float objectY)
        {
            return InputLoader.MouseLoader.NewMouse.Y -objectY;
        }

    }
}
