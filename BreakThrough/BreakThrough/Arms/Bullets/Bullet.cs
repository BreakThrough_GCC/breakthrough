﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BreakThrough.Arms.Bullets
{
    abstract class Bullet
    {
        public byte Power { get; protected set; }
        public Vector3 Position { get; protected set; }
        protected Vector3 Speed { get; set; }
    }
}
