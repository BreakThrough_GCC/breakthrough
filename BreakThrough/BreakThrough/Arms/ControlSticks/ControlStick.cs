﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakThrough.Arms.ControlSticks
{
    abstract class ControlStick
    {
        public virtual bool ManualHandling { get; protected set; }
        public virtual bool ManualAming { get; protected set; }
    }
}
