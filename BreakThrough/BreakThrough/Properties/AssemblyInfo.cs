using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// アセンブリに関する全般的な情報は、以下の一連の属性によって管理されます。 
// アセンブリに関連付けられている情報を変更するには、これらの属性値を変更します。
// 
[assembly: AssemblyTitle("BreakThrough")]
[assembly: AssemblyProduct("BreakThrough")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyCompany("ANCT Game Creators' Circle")]
[assembly: AssemblyCopyright("Copyright © ANCTGCC 2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// ComVisible 属性を False に設定すると、このアセンブリに含まれる型が 
// COM コンポーネントに公開されません。COM からこのアセンブリの型にアクセスする必要がある場合は、 
// その型の ComVisible 属性を True に設定してください。
// Windows アセンブリだけが COM に対応しています。
[assembly: ComVisible(false)]

// Windows 上では、このプロジェクトが COM に公開されている場合、 次の GUID が typelib の ID として設定されます。
// Windows 以外のプラットフォームでは、このアセンブリをデバイスに展開するときに
// タイトル ストレージ コンテナーを一意に識別するために使用します。
[assembly: Guid("d0af8868-1065-47a6-b7ae-7cdefb9a8911")]

// アセンブリのバージョン情報は、次の 4 つの値で構成されています。
//
//      メジャー バージョン
//      マイナー バージョン 
//      ビルド番号
//      リビジョン番号
//
[assembly: AssemblyVersion("0.1.*")]
