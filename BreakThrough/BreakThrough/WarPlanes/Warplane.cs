﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BreakThrough.Warplanes
{
    abstract class Warplane
    {
        protected Arms.Weapons.Weapon Weapon { get; set; }
        protected Arms.Armors.Armor Armor { get; set; }
        public Arms.ControlSticks.ControlStick ControlStick { get; protected set; }
        public Vector3 Position { get; protected set; }

        public abstract void Shoot();
    }
}
