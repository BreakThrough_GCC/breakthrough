﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakThrough.Factory
{
    abstract class PlanesFactory
    {
        public abstract Warplanes.Warplane MakePlane(Warplanes.Warplane plane);

        protected abstract Arms.Weapons.Weapon MakeWeapon();

        protected abstract Arms.Armors.Armor MakeArmor();

        protected abstract Arms.ControlSticks.ControlStick MakeControlStick();
    }
}
