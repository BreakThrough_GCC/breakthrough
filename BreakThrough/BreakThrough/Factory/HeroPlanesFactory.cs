﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakThrough.Factory
{
    abstract class HeroPlanesFactory : PlanesFactory
    {
        protected abstract Arms.Weapons.Weapon MakeSubWeapon();
    }
}
